git基本教程以及工作时的常用命令

git教程可以参考 git.oschina上的使用教程，以前我整理过的还可以在teambition上的“挑战脂肪肝”>“分享墙”里面找到

git学习资料、路线及经验等
更多
建议大家装完git环境后，在自己的机器上多做做练习，以命令行为基础，然后再用图形客户端。

扫盲：

---------------------------

1.不管三七二十一先上手就看这个“git-quick-start”

http://git.oschina.net/wzw/git-quick-start

2.看廖雪峰的git教程（了解git历史以及git的必要性等，可以仔细看）

http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000

3.猴子都能看懂的git教程

http://backlogtool.com/git-guide/cn/

入门：

---------------------------

1.扫完盲后，看一些比较高级的教程

http://marklodato.github.io/visual-git-guide/index-zh-cn.html

2.上手练习（英语好的推荐，英语不好的也推荐）

https://try.github.io/levels/1/challenges/1

3.一个国内网站总结的教程，比较全，可参考

http://www.yiibai.com/git/home.html

4.一个程序猿写的git教程、跟着看下来操作一遍也就会了。

http://www.cnblogs.com/tugenhua0707/p/4050072.html

其他：

---------------------------

0.git.oschina上的帮助

http://git.oschina.net/oschina/git-osc/wikis/Home

1.Git分支实时互动教程

http://pcottle.github.io/learnGitBranching/?demo

2.GITHUB官方视频教程

http://www.stuq.org/course/detail/969

2.git magic教程

http://www-cs-students.stanford.edu/~blynn/gitmagic/intl/zh_cn/index.html

3.最权威的PROGIT（中文版）

http://git.oschina.net/progit/

英文版见git官网 git-scm.com

4.一些图形客户端的使用

http://www.ui.cn/detail/20957.html

5.git常用命令（git cheat sheet)

https://github.com/arslanbilal/git-cheat-sheet (英文)

https://github.com/flyhigher139/Git-Cheat-Sheet (中文)

6.直接可以用的工具网站

git.oschina.net (推荐，国内的速度快，界面友好全中文，可以建私有项目)

github.com (国外的，自己建的项目是公开的，建私有项目要收费)

gitlab.com (国外的，可以建私有项目，跟git.oschina一样)

注意：尽量把git升级到最新版本，从[官网](http://git-scm.com)下载。
